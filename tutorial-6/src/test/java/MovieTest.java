import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

public class MovieTest {
    private Customer customer;
    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");
        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void testEqual() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.CHILDREN);
        Movie movie3 = movie;
        Movie movie2 = new Movie("Who Killed Caption Alex?", Movie.CHILDREN);

        assertEquals(movie.equals(movie3),true);
        assertEquals(movie.equals(10),false);
        assertEquals(movie.equals(movie2),false);
    }

    @Test
    public void testHashCode() {
        Movie movie = new Movie("Who Killed Captain Alex?", Movie.CHILDREN);
        Movie movie2 = new Movie("Who Killed Captain Alex 2?", Movie.CHILDREN);

        assertNotEquals(movie.hashCode(), movie2.hashCode());
    }
}