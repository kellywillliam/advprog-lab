package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary < 90000.00) {
            throw new IllegalArgumentException(
                "UiUxDesigner Salary must not lower than 90000.00");
        }
        this.salary = salary;
        this.role = "UI/UX Designer";
    }
    
    @Override
    public double getSalary() {
        return this.salary;
    }
}
