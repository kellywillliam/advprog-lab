package tutorial.javari;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.simpleflatmapper.csv.CsvParser;
import org.simpleflatmapper.csv.CsvWriter;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tutorial.javari.animal.Animal;


@RestController
public class JavariController {
    public static ArrayList<Animal> animals = new ArrayList<>();
    public File file;
    CsvWriter.CsvWriterDSL<Animal> writer = CsvWriter.from(Animal.class)
            .columns("id","type","name","length","weight","gender","condition");

    public JavariController() throws IOException {
        file = new File("./tutorial-9/src/main/java/tutorial/javari/animal_records.csv");
        CsvParser.mapTo(Animal.class).forEach(file,animals::add);

    }

    //get all Animals in Javari
    @RequestMapping(value = "/javari", method = RequestMethod.GET)
    public ArrayList<Animal> getAnimals() {
        return animals;
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.GET)
    public Object[] getAnimalById(@PathVariable("id") int id) {
        Stream<Animal> getSpecificAnimal = animals.stream()
                .filter(animal -> animal.getId() == id);
        if (animals.size() < id || id < 0) {
            String[] noAnimalWithId = new String[1];
            noAnimalWithId[0] = "Animal with ID " + id
                    + " is nowhere to be found.";
            return (Object[]) noAnimalWithId;
        } else {
            return getSpecificAnimal.toArray();
        }
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public Object[] deleteAnimalById(@PathVariable("id") int id) {
        Stream<Animal> getSpecificAnimal = animals.stream()
                .filter(animal -> animal.getId() == id);
        if (animals.size() < id || id < 0) {
            String[] noAnimalWithId = new String[1];
            noAnimalWithId[0] = "Fail to delete Animal with ID " + id
                    + " because the Animal isn't here in the first place.";
            return (Object[]) noAnimalWithId;
        }
        animals.remove(id);
        return getSpecificAnimal.toArray();
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public void addAnimal(@RequestBody Animal animal) {
        animals.add(animal);
    }
}
