import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class ScoreGroupingTest {
    private static Map<String, Integer> scores = new HashMap<>();

    @Before
    public void setUp() {
        scores.put("Daisy", 9);
        scores.put("Andy", 12);
        scores.put("Bobby", 11);
        scores.put("Anthony", 13);
    }

    @Test
    public void printAllScores() {
        assertEquals("{9=[Daisy], 11=[Bobby], 12=[Andy], 13=[Anthony]}",
                ScoreGrouping.groupByScores(scores).toString());
    }

    @Test
    public void testMain() {
        ScoreGrouping.main(new String[]{});
    }
}