package sorting;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import org.junit.Test;

public class SortAndSearchTest {
    int[] arrTester = {5,4,2,7,9,3,1,8,6,10};

    @Test
    public void sortTest() {
        int[] slowSort = Sorter.slowSort(arrTester);
        int[] fastSort = Sorter.quickSort(arrTester);
        assertTrue(Arrays.equals(slowSort,fastSort));
    }

    @Test
    public void findTest() {
        int slowFinder = Finder.slowSearch(arrTester,3);
        int fastFinder = Finder.fastSearch(arrTester,3);
        assertTrue(slowFinder == fastFinder);
    }
}
