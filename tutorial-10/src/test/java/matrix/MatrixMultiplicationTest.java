package matrix;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import org.junit.Test;

public class MatrixMultiplicationTest {
    double[][] square1 = {{1,3}, {2,4}};
    double[][] square2 = {{5,7}, {6,8}};

    double[][] squareResult = {{23,31}, {34,46}};
    double[][] result;

    @Test
    public void testSquareBasicMultiplication() throws InvalidMatrixSizeForMultiplicationException {
        result = MatrixOperation.basicMultiplicationAlgorithm(square1,square2);
        assertTrue(Arrays.deepEquals(result, squareResult));
    }

    @Test
    public void testSquareStrassensMultiplication() {
        result = MatrixOperation.strassenMatrixMultiForNonSquareMatrix(square1, square2);
        assertTrue(Arrays.deepEquals(result, squareResult));
    }
}

