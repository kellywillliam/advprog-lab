package game;

public class QuizTimer implements Runnable {
    private QuizScorer quizScorer;
    private Thread thread;
    private int sleepTime = 1000;

    public QuizTimer(QuizScorer quizScorer) {
        this.quizScorer = quizScorer;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, "Quiz Timer Checker");
            thread.start();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                Thread.sleep(sleepTime);
                quizScorer.decrement();
            }
        } catch (InterruptedException e) {
            System.out.println("Quiz Timer interrupted.");
        }
        System.out.println("Quiz Timer closed.");
    }
}
