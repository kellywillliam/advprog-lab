package game;

public class QuizScorer {
    private double score = 100;

    synchronized void increment(double value) {
        score += value;
    }

    synchronized void decrement() {
        score--;
    }

    public double value() {
        return score;
    }
}
