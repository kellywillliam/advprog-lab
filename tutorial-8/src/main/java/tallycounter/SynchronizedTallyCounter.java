package tallycounter;

public class SynchronizedTallyCounter {
    private int counter = 0;

    synchronized void increment() {
        counter++;
    }

    synchronized void decrement() {
        counter--;
    }

    public int value() {
        return counter;
    }
}
