package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {

        model.addAttribute("cover","=============================================================");
        if (name.equals("") || name == null) {
            model.addAttribute("title", "This is My CV!");
        } else {
            model.addAttribute("title", "Hello " + name + ", I hope you're interested with me!");
        }

        model.addAttribute("header1", "BIODATA");
        model.addAttribute("nama", "Name: Kelly William");
        model.addAttribute("birthdate", "Birthdate: 14 May 1999");
        model.addAttribute("birthplace", "Birthplace: Medan");
        model.addAttribute("address", "Address: Margonda Residence 1");

        model.addAttribute("header2", "EDUCATION");
        model.addAttribute("highschool", "High School: SMAS Sutomo 1 Medan");
        model.addAttribute("current", "Currently an Undergraduate Computer Science Student at "
                + "Faculty of Computer Science University of Indonesia");

        model.addAttribute("header3", "SUMMARY");
        model.addAttribute("desc1", "Currently an Undergraduate Computer Student. "
                + "Currently a Teaching Assistant for Introduction of Digital System. "
                + "Currently a Secretary for CompFest X and Asciipella. "
                + "Likes to eat, play, sleep, repeat");

        return "greeting";
    }
}
