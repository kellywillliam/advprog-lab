package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class CrispyCrustDough implements Dough {
    public String toString() {
        return "CrispyCrust style extra crispy crust dough";
    }
}
