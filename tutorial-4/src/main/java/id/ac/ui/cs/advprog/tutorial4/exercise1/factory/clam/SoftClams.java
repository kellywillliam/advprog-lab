package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SoftClams implements Clams {

    public String toString() {
        return "Soft Clams from New England";
    }
}
